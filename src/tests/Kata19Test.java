package tests;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import sabre.Kata19;
import sabre.Word;

/**
* <h1>Kata19Test</h1>
* This class implements all the unit tests of the project.
*
* @author  Antonio Lacerda
* @version 1.0
* @since   2017-08-27
*/
public class Kata19Test {

    @Before
    public void setUp() throws Exception {
        Kata19.init();
        Kata19.addWords();
    }
    
    @Test
    public void testPath0() {
        List<Word> path = Kata19.searchPath("mobile", "device");
        assertTrue(path.size() == 0);
    }

    @Test
    public void testPath2() {
        List<Word> path = Kata19.searchPath("make", "take");
        assertTrue(path.size() == 2);
    }

    @Test
    public void testPath3() {
        List<Word> path = Kata19.searchPath("phone", "shine");
        assertTrue(path.size() == 3);
    }

    @Test
    public void testPath4() {
        List<Word> path = Kata19.searchPath("put", "bog");
        assertTrue(path.size() == 4);
    }
    
    @Test
    public void testPath5() {
        List<Word> path = Kata19.searchPath("ruby", "side");
        assertTrue(path.size() == 5);
    }
    
    @Test
    public void testPath6() {
        List<Word> path = Kata19.searchPath("ruby", "code");
        assertTrue(path.size() == 6);
    }
}
