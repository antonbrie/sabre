package sabre;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
* <h1>Kata19</h1>
* This class resolves the problem 'Kata 19'.
* 
* The strategy to resolve the problem consist in calculating two distances for each word (of the same length):
* 
* distance1: in relation to the initial word;
* distance2: in relation to the final word;
* 
* After this, walk one step ahead, that is, find another word to replace the initial word and recalculating distance1 for all words.
* 
* Repeat the previous step until to find the final word, otherwise there is no path.
*
* @author  Antonio Lacerda
* @version 1.0
* @since   2017-08-27
*/
public class Kata19 {
    private static List<Word> path = new ArrayList<Word>();
    private static List<Words> lstWords = new ArrayList<Words>();
    
    public static void main(String[] args) {
        if(args.length >= 2) {
            String ref1 = args[0];
            String ref2 = args[1];
            if(ref1.length() == ref2.length()) {
                init();
                addWords();
                //print();
                path = searchPath(ref1, ref2);
            } else {
                System.out.println("The two words must have the same length.");
            }
            for(Word word : path) {
                System.out.print(word.getWord() + " ");
            }
            System.out.println();
        }
    }
    
    public static List<Word> searchPath(String ref1, String ref2) {
        path.clear();
        for(Words words : lstWords) {
            if(words.getLength() == ref1.length()) {
                words.setReference1(ref1);
                words.setReference2(ref2);
                path.add(words.getWord(ref1));
                Word nearest = words.getNearest();
                int count = 0;
                while(nearest != null && !nearest.getWord().equals(ref2)) {
                    path.add(nearest);
                    words.setReference1(nearest.getWord());
                    nearest = words.getNearest();
                    count++;
                    if(count > words.getLength()) {
                        break;
                    }
                }
                if(nearest != null) {
                    path.add(nearest);
                }
                if(count > words.getLength() || nearest == null) {
                    System.out.println("There is no path.");
                    path.clear();
                }
                break;
            }
        }
        return path;
    }
    
    public static void init() {
        for(int i = 2; i <= Constants.MAX_LENGTH; i++) {
            lstWords.add(new Words(i));
        }
    }
    
    public static void print() {
        int total = 0;
        for(Words words : lstWords) {
            total += words.print();
        }
        System.out.println("Total = " + total);
    }
    
    public static void addWords() {
        try {
            Stream<String> list = Files.lines(Paths.get(Constants.PATH));
            list.forEach(Kata19::addWord);
            list.close();
        } catch(Exception e) {
            System.out.println("Unkown error reading words.");
            e.printStackTrace();
        }
    }
    
    public static void addWord(String line) {
        if(line != null && !line.trim().equals("")) {
            int l = line.length();
            if(l > 1) {
                for(Words words : lstWords) {
                    if(words.getLength() == l) {
                        words.add(line);
                    }
                }
            }
        }
    }
}
