package sabre;

import java.util.ArrayList;
import java.util.List;

/**
* <h1>Words</h1>
* This class stores a list of words of same length.
*
* @author  Antonio Lacerda
* @version 1.0
* @since   2017-08-27
*/
public class Words {
    private int length;
    
    private List<Word> words;
    
    private String ref1;
    private String ref2;
    
    public Words(int length) {
        words = new ArrayList<Word>();
        this.length = length;
        String ref1 = "";
        for(int i = 0; i < length; i++) {
            ref1 += 'a';
        }
        setReference1(ref1);
        String ref2 = "";
        for(int i = 0; i < length; i++) {
            ref2 += 'z';
        }
        setReference2(ref2);
    }
    
    public int getLength() {
        return this.length;
    }
    
    public List<Word> getWords() {
        return this.words;
    }
    
    public void add(String word) {
        if(word != null && word.length() == length && !hasInvalidChar(word)) {
            Word w = new Word(word);
            int d1 = distance(word, ref1);
            int d2 = distance(word, ref2);
            w.setDistance1(d1);
            w.setDistance2(d2);
            words.add(w);
        }
    }
    
    public Word getWord(String word) {
        if(word != null) {
            for(Word w : words) {
                if(w.getWord().equals(word)) {
                    return w;
                }
            }
        }
        return null;
    }
    
    public int distance(String w1, String w2) {
        int distance = 0;
        if(w1 != null && w2 != null) {
            for(int i = (length-1); i >= 0; i--) {
                if(w1.charAt(i) != w2.charAt(i)) {
                    distance++;
                }
            }
        } else {
            distance = Constants.BIG_DISTANCE;
        }
        return distance;
    }
    
    private void setDistances1() {
        for(Word word : words) {
            int d1 = distance(word.getWord(), ref1);
            word.setDistance1(d1);
        }
    }
    
    private void setDistances2() {
        for(Word word : words) {
            int d2 = distance(word.getWord(), ref2);
            word.setDistance2(d2);
        }
    }
    
    public void setReference1(String ref1) {
        this.ref1 = ref1;
        setDistances1();
    }
    
    public void setReference2(String ref2) {
        this.ref2 = ref2;
        setDistances2();
    }
    
    public int getShortestDistance() {
        int shortestDistance = Constants.BIG_DISTANCE;
        for(Word word : words) {
            if(!word.getWord().equals(ref1) && !word.getWord().equals(ref2)) {
                int distance = word.getDistance1() + word.getDistance2();
                if(distance < shortestDistance) {
                    shortestDistance = distance;
                }
            }
        }
        return shortestDistance;
    }
    
    public int print() {
        int size = words.size();
        System.out.println("Amount of words of length: " + length);
        System.out.println(">>> " + size);
        System.out.println();
        return size;
    }
    
    public void printWords() {
        for(Word word : words) {
            word.print();
        }
        System.out.println("Shortest distance = " + getShortestDistance());
    }
    
    public List<Word> getCandidates() {
        List<Word> candidates = new ArrayList<Word>();
        int shortest = getShortestDistance();
        for(Word word : words) {
            int d1 = word.getDistance1();
            int d2 = word.getDistance2();
            int d = d1 + d2;
            if(d <= shortest) {
                candidates.add(word);
            }
        }
        return candidates;
    }
    
    public Word getNearest() {
        Word nearest = null;
        List<Word> candidates = getCandidates();
        for(Word word : candidates) {
            int d1 = word.getDistance1();
            int d2 = word.getDistance2();
            if(d1 == 1 && d2 == 0) {
                return word;
            }
        }
        for(Word word : candidates) {
            if(word.getDistance1() == 1) {
                return word;
            }
        }
        return nearest;
    }
    
    private boolean hasInvalidChar(String word) {
        if(word.contains("A")) return true;
        if(word.contains("B")) return true;
        if(word.contains("C")) return true;
        if(word.contains("D")) return true;
        if(word.contains("E")) return true;
        if(word.contains("F")) return true;
        if(word.contains("G")) return true;
        if(word.contains("H")) return true;
        if(word.contains("I")) return true;
        if(word.contains("J")) return true;
        if(word.contains("K")) return true;
        if(word.contains("L")) return true;
        if(word.contains("M")) return true;
        if(word.contains("N")) return true;
        if(word.contains("O")) return true;
        if(word.contains("P")) return true;
        if(word.contains("Q")) return true;
        if(word.contains("R")) return true;
        if(word.contains("S")) return true;
        if(word.contains("T")) return true;
        if(word.contains("U")) return true;
        if(word.contains("V")) return true;
        if(word.contains("W")) return true;
        if(word.contains("X")) return true;
        if(word.contains("Y")) return true;
        if(word.contains("Z")) return true;
        if(word.contains("'")) return true;
        return false;
    }
}
