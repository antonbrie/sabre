package sabre;

/**
* <h1>Word</h1>
* This class stores all relevant information about a particular word.
*
* @author  Antonio Lacerda
* @version 1.0
* @since   2017-08-27
*/
public class Word {
    private String word;
    private int distance1;
    private int distance2;
    
    public Word(String word) {
        this.word = word;
        this.distance1 = Constants.BIG_DISTANCE;
        this.distance2 = Constants.BIG_DISTANCE;
    }

    public String getWord() {
        return word;
    }

    public int getDistance1() {
        return distance1;
    }

    public int getDistance2() {
        return distance2;
    }
    
    public void setDistance1(int distance1) {
        this.distance1 = distance1;
    }
    
    public void setDistance2(int distance2) {
        this.distance2 = distance2;
    }
    
    public void print() {
        System.out.println(word + " (" + distance1 + ") (" + distance2 + ")");
    }
}
