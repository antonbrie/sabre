package sabre;

/**
* <h1>Constants</h1>
* This class stores all constants of the project.
*
* @author  Antonio Lacerda
* @version 1.0
* @since   2017-08-27
*/
public class Constants {
    public static final String PATH = ".//data//wordlist.txt";
    public static final int MAX_LENGTH = 60;
    public static final int BIG_DISTANCE = 1000000;
}
